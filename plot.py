#!/usr/bin/python

from datetime import date
import cms, atlas
import os
import ROOT

# ROOT setup
dirname = os.path.join(os.path.dirname(__file__), "./atlasrootstyle")
ROOT.gROOT.SetBatch(True)
ROOT.gROOT.LoadMacro(os.path.join(dirname, "AtlasStyle.C"))
ROOT.gROOT.LoadMacro(os.path.join(dirname, "AtlasLabels.C"))
ROOT.gROOT.LoadMacro(os.path.join(dirname, "AtlasUtils.C"))
ROOT.SetAtlasStyle()
ROOT.gStyle.SetEndErrorSize(8)

def item_by_order(dict, order):
    for k, v in dict.items():
        if v["order"] == order:
            return k, v
    return None, None

def createCanvasPads(name):
    c = ROOT.TCanvas(name, name, 1200, 1400)
    # Left
    pad1 = ROOT.TPad(f"pad1_{name}", f"pad1_{name}", 0, 0.0, 0.4, 1.0)
    pad1.SetRightMargin(0.0)
    pad1.SetFillStyle(4000)
    pad1.Draw()
    # Right
    c.cd()
    pad2 = ROOT.TPad(f"pad2_{name}", f"pad2_{name}", 0.4, 0.0, 1.0, 1.0)
    pad2.SetLeftMargin(0.0)
    pad2.SetFillStyle(4000)
    pad2.Draw()
    # Left zoomed-out
    pad3 = ROOT.TPad(f"pad3_{name}", f"pad3_{name}", 0.008, 0.15, 0.395, 0.51)
    pad3.SetRightMargin(0.0)
    pad3.SetFillStyle(0)
    pad3.SetFillColor(0)
    pad3.Draw()

    return c, pad1, pad2, pad3

# TGraphs
y = 1.0
gr_tot = ROOT.TGraphAsymmErrors()
gr_stat = ROOT.TGraphAsymmErrors()
gr_sign = ROOT.TGraphAsymmErrors()
gr_atlas_tot = ROOT.TGraphAsymmErrors()
gr_atlas_stat = ROOT.TGraphAsymmErrors()
gr_atlas_sign = ROOT.TGraphAsymmErrors()
gr_partial_tot = ROOT.TGraphAsymmErrors()
gr_partial_stat = ROOT.TGraphAsymmErrors()
gr_partial_sign = ROOT.TGraphAsymmErrors()
gr_partial_atlas_tot = ROOT.TGraphAsymmErrors()
gr_partial_atlas_stat = ROOT.TGraphAsymmErrors()
gr_partial_atlas_sign = ROOT.TGraphAsymmErrors()
mg1 = ROOT.TMultiGraph()
mg2 = ROOT.TMultiGraph()

cms_data = cms.data
atlas_data = atlas.data

for i in range(len(cms_data)):

    # CMS data
    k, v = item_by_order(cms_data, i+1)
    if v["data_fb"] > 100:
        temp_gr_tot = gr_tot
        temp_gr_stat = gr_stat
        temp_gr_sign = gr_sign
    else:
        temp_gr_tot = gr_partial_tot
        temp_gr_stat = gr_partial_stat
        temp_gr_sign = gr_partial_sign
    mu_sig = v["mu_sig"]
    if i < 50:
        temp_gr_tot.SetPoint(temp_gr_tot.GetN(), mu_sig, y)
        temp_gr_tot.SetPointError(temp_gr_tot.GetN()-1, v["mu_sig_err_tot"][0], v["mu_sig_err_tot"][1], 0, 0)
        if v["mu_sig_err_stat"]:
            temp_gr_stat.SetPoint(temp_gr_stat.GetN(), mu_sig, y)
            temp_gr_stat.SetPointError(temp_gr_stat.GetN()-1, v["mu_sig_err_stat"][0], v["mu_sig_err_stat"][1], 0, 0)
    temp_gr_sign.SetPoint(temp_gr_sign.GetN(), 0, y)
    if mu_sig > 1.0:
        temp_gr_sign.SetPointError(temp_gr_sign.GetN()-1, 0, max(0.1, (mu_sig - 1.0) / v["mu_sig_err_tot"][0]), 0.0, 0.0)
    else:
        temp_gr_sign.SetPointError(temp_gr_sign.GetN()-1, max(0.1, (1.0 - mu_sig) / v["mu_sig_err_tot"][1]), 0.0, 0.0, 0.0)

    # ATLAS data
    k_atlas, v_atlas = item_by_order(atlas_data, i+1)
    if v_atlas:
        k, v, = k_atlas, v_atlas
        if v["data_fb"] > 100:
            temp_gr_tot = gr_atlas_tot
            temp_gr_stat = gr_atlas_stat
            temp_gr_sign = gr_atlas_sign
        else:
            temp_gr_tot = gr_partial_atlas_tot
            temp_gr_stat = gr_partial_atlas_stat
            temp_gr_sign = gr_partial_atlas_sign
        mu_sig = v["mu_sig"]
        if i < 50:
            temp_gr_tot.SetPoint(temp_gr_tot.GetN(), mu_sig, y - 0.02)
            temp_gr_tot.SetPointError(temp_gr_tot.GetN()-1, v["mu_sig_err_tot"][0], v["mu_sig_err_tot"][1], 0, 0)
            if v["mu_sig_err_stat"]:
                temp_gr_stat.SetPoint(temp_gr_stat.GetN(), mu_sig, y - 0.02)
                temp_gr_stat.SetPointError(temp_gr_stat.GetN()-1, v["mu_sig_err_stat"][0], v["mu_sig_err_stat"][1], 0, 0)

        temp_gr_sign.SetPoint(temp_gr_sign.GetN(), 0, y - 0.02)
        if mu_sig > 1.0:
            temp_gr_sign.SetPointError(temp_gr_sign.GetN()-1, 0, max(0.1, (mu_sig - 1.0) / v["mu_sig_err_tot"][0]), 0.0, 0.0)
        else:
            temp_gr_sign.SetPointError(temp_gr_sign.GetN()-1, max(0.1, (1.0 - mu_sig) / v["mu_sig_err_tot"][1]), 0.0, 0.0, 0.0)

    y -= 0.1


for gr in [gr_tot, gr_stat, gr_sign, gr_atlas_tot, gr_atlas_stat, gr_atlas_sign, gr_partial_tot, gr_partial_stat, gr_partial_sign, gr_partial_atlas_tot, gr_partial_atlas_stat, gr_partial_atlas_sign]:
    gr.SetLineWidth(2)

for gr in [gr_partial_tot, gr_partial_stat, gr_partial_sign]:
    gr.SetLineColor(ROOT.kGray+2)
    gr.SetMarkerColor(ROOT.kGray+2)

for gr in [gr_atlas_tot, gr_atlas_stat, gr_atlas_sign]:
    gr.SetLineColor(ROOT.kBlue+1)
    gr.SetMarkerColor(ROOT.kBlue+1)

for gr in [gr_partial_atlas_tot, gr_partial_atlas_stat, gr_partial_atlas_sign]:
    gr.SetLineColor(ROOT.kBlue-7)
    gr.SetMarkerColor(ROOT.kBlue-7)

for gr in [gr_tot, gr_atlas_tot, gr_partial_tot, gr_partial_atlas_tot]:
    if gr.GetLineColor() == ROOT.kBlack:
        gr.SetLineColor(ROOT.kGray+3)
        gr.SetMarkerColor(ROOT.kGray+3)
    else:
        gr.SetLineColor(gr.GetLineColor() - 1)
        gr.SetMarkerColor(gr.GetMarkerColor() - 1)

mg1.Add(gr_atlas_tot, "pe0")
mg1.Add(gr_atlas_stat, "pe0")
mg1.Add(gr_partial_atlas_tot, "pe0")
mg1.Add(gr_partial_atlas_stat, "pe0")
mg1.Add(gr_partial_tot, "pe0")
mg1.Add(gr_partial_stat, "pe0")
mg1.Add(gr_tot, "pe0")
mg1.Add(gr_stat, "pe0")


MINIMUM = -0.6
MAXIMUM = 1.4
gr_1sigma = ROOT.TGraphAsymmErrors()
gr_1sigma.SetPoint(0, 0.0, 0.9*MAXIMUM)
gr_1sigma.SetPointError(0, 1.0, 1.0, 0.9*MAXIMUM - MINIMUM, 0.0)
gr_1sigma.SetFillColor(ROOT.kGreen+1)
gr_2sigma = ROOT.TGraphAsymmErrors()
gr_2sigma.SetPoint(0, 0.0, 0.9*MAXIMUM)
gr_2sigma.SetPointError(0, 2.0, 2.0, 0.9*MAXIMUM - MINIMUM, 0.0)
gr_2sigma.SetFillColor(ROOT.kYellow)

for gr in [gr_sign, gr_atlas_sign]:
    gr.SetLineWidth(2)

mg2.Add(gr_2sigma, "e2")
mg2.Add(gr_1sigma, "e2")
mg2.Add(gr_partial_atlas_sign, "e")
mg2.Add(gr_atlas_sign, "e")
mg2.Add(gr_partial_sign, "e")
mg2.Add(gr_sign, "e")

# Proxy axis
h1 = ROOT.TH1F("h1", "h1", 1, -0.1, 1.99)
h1.SetBinContent(1, -999)
h1.SetMinimum(MINIMUM)
h1.SetMaximum(MAXIMUM)
h1.GetYaxis().SetLabelSize(0)
h1.GetXaxis().SetTitle("#it{#mu}_{SIG} = #frac{#sigma_{#it{H}}#times#it{B}_{#it{H}#rightarrowbb/cc}}{#sigma_{#it{H}}^{SM}#times#it{B}_{#it{H}#rightarrowbb/cc}^{SM}}")
h1.GetXaxis().SetNdivisions(505)
h1.GetYaxis().SetNdivisions(0)

h2 = ROOT.TH1F("h2", "h2", 1, -3.9, 8.9)
h2.SetBinContent(1, -999)
h2.SetMinimum(MINIMUM)
h2.SetMaximum(MAXIMUM)
h2.GetYaxis().SetLabelSize(0)
h2.GetXaxis().SetTitle("(1.0 - #it{#mu}_{SIG}) / #sigma_{#it{#mu}_{SIG}}")
h2.GetYaxis().SetNdivisions(0)

h3 = ROOT.TH1F("h3", "h3", 1, -50, 47)
h3.SetBinContent(1, -999)
h3.SetMinimum(MINIMUM + 0.13)
h3.SetMaximum(0.24)
h3.GetYaxis().SetLabelSize(0)
# h3.GetXaxis().SetTitle("#it{#mu}_{SIG}")
h3.GetXaxis().SetNdivisions(505)
h3.GetYaxis().SetNdivisions(0)

# Canvas
canv, pad1, pad2, pad3 = createCanvasPads("canv")
scale = pad2.GetWNDC()*pad2.GetWw() / (pad1.GetWNDC()*pad1.GetWw())
pad1.cd()
h1.GetXaxis().SetLabelSize(1.3 * h1.GetXaxis().GetLabelSize() * scale)
h1.GetXaxis().SetLabelOffset(-0.033)
h1.GetXaxis().SetTitleSize(1.2 * h1.GetXaxis().GetTitleSize() * scale)
h1.GetXaxis().SetTitleOffset(1.0 / scale)
h1.Draw()
line1 = ROOT.TLine(1.0, MINIMUM, 1.0, 1.2)
line1.SetLineStyle(2)
line1.Draw()
mg1.Draw("")
ROOT.gPad.RedrawAxis()

pad2.cd()
h2.GetXaxis().SetLabelSize(1.3 * h2.GetXaxis().GetLabelSize())
h2.GetXaxis().SetLabelOffset(-0.01)
h2.GetXaxis().SetTitleSize(1.2 * h2.GetXaxis().GetTitleSize())
h2.GetXaxis().SetTitleOffset(0.8)
h2.Draw()
mg2.Draw("")
lines = []
# for s in [-3, -2, -1, 1, 2]:
#     line = ROOT.TLine(s, MINIMUM, s, 1.2)
#     line.SetLineStyle(2)
#     line.Draw()
#     lines += [line]
line2 = ROOT.TLine(0.0, MINIMUM, 0.0, 1.2)
line2.SetLineStyle(1)
line2.Draw()

pad3.cd()
h3.Draw()
h3.GetXaxis().SetLabelOffset(-0.08)
h3.GetXaxis().SetLabelSize(h3.GetXaxis().GetLabelSize() * 1.2)
line3 = ROOT.TLine(1.0, MINIMUM + 0.2, 1.0, 0.24)
line3.SetLineStyle(2)
line3.Draw()
mg1.Draw("")

# Labels
pad2.cd()
y = 1.0
for i in range(len(cms_data)):
    k, v = item_by_order(cms_data, i+1)
    k_atlas, v_atlas = item_by_order(atlas_data, i+1)

    # Analysis name
    label = ROOT.TLatex()
    label.SetTextFont(53)
    label.SetTextAlign(11)
    label.SetTextSize(20)
    if v["data_fb"] > 100:
        label.SetTextColor(ROOT.kBlack)
    else:
        label.SetTextColor(ROOT.kGray+2)
    label.DrawLatex(2.5, y, f"{v['reference']}, {v['data_fb']} fb^{{-1}}")

    if v_atlas:
        k, v, = k_atlas, v_atlas
        # Analysis reference
        label = ROOT.TLatex()
        label.SetTextFont(53)
        label.SetTextAlign(11)
        label.SetTextSize(20)
        if v["data_fb"] > 100:
            label.SetTextColor(ROOT.kBlue)
        else:
            label.SetTextColor(ROOT.kBlue-7)
        label.DrawLatex(2.5, y - 0.042, f"{v['reference']}, {v['data_fb']} fb^{{-1}}")
    y -= 0.1
ROOT.gPad.RedrawAxis()

# global labels
# -0.1, 1.99
# -3.9, 8.9
lines = []
for y in [0.85, 0.75, 0.65, 0.25, 0.05, -0.05, -0.25]:
    if y > 0.05:
        pad1.cd()
        l1 = ROOT.TLine(-0.1, y, 1.99, y)
        l1.SetLineStyle(2)
        l1.Draw()
        lines += [l1]

    pad2.cd()
    l2 = ROOT.TLine(-3.9, y, 8.9, y)
    l2.SetLineStyle(2)
    l2.Draw()
    lines += [l2]

pad2.cd()
for y, name in zip([1.05, 0.85, 0.75, 0.65, 0.25, 0.05, -0.05, -0.25], ["VHbb", "#splitline{VHbb}{m(bb) fit}", "VBF Hbb", "ttHbb", "#splitline{Hbb}{boosted}", "#splitline{VBF Hbb}{boosted}", "VHcc", "#splitline{VHcc}{boosted}"]):
    # Analysis name
    label = ROOT.TLatex()
    label.SetTextFont(43)
    label.SetTextAlign(13)
    label.SetTextSize(26)
    label.DrawLatex(-3.8, y - 0.01, name)

# Legend
grs = []
pad1.cd()
box1 = ROOT.TBox(-0.1 + 0.05, 1.05, 1.99 - 0.05, 1.3)
box1.SetFillColor(ROOT.kWhite)
box1.SetLineWidth(0)
box1.Draw()

temp_gr = ROOT.TGraphErrors()
temp_gr.SetPoint(0, 0.35, 1.35 - 0.08*1)
temp_gr.SetPointError(0, 0.25, 0.0)
temp_gr.SetLineWidth(2)
temp_gr.SetLineColor(ROOT.kGray+3)
temp_gr.SetMarkerColor(ROOT.kGray+3)
temp_gr.Draw("pe")
grs += [temp_gr]

temp_gr_stat = ROOT.TGraphErrors()
temp_gr_stat.SetPoint(0, 0.35, 1.35 - 0.08*1)
temp_gr_stat.SetPointError(0, 0.15, 0.0)
temp_gr_stat.SetLineWidth(2)
temp_gr_stat.SetLineColor(ROOT.kBlack)
temp_gr_stat.SetMarkerColor(ROOT.kBlack)
temp_gr_stat.Draw("pe")
grs += [temp_gr_stat]

label = ROOT.TLatex()
label.SetTextFont(43)
label.SetTextAlign(12)
label.SetTextSize(28)
label.SetTextColor(ROOT.kBlack)
label.DrawLatex(0.7, 1.35 - 0.08*1, "Total & Stat. Unc.")

pad2.cd()
box2 = ROOT.TBox(-3.9 + 0.05, 1.05, 8.9 - 0.05, 1.4 - 0.005)
box2.SetFillColor(ROOT.kWhite)
box2.SetLineWidth(0)
box2.Draw()

n = 0
for c1, c2, name in zip([ROOT.kGray+3, ROOT.kGray+1, ROOT.kBlue, ROOT.kBlue-8],
                        [ROOT.kBlack, ROOT.kGray+2, ROOT.kBlue+1, ROOT.kBlue-7],
                        ["CMS full Run 2", "CMS partial", "ATLAS full Run 2", "ATLAS partial"]):

    temp_gr = ROOT.TGraphErrors()
    temp_gr.SetPoint(0, -2.5, 1.35 - 0.08*n)
    temp_gr.SetPointError(0, 0.75, 0.0)
    temp_gr.SetLineWidth(2)
    temp_gr.SetLineColor(c1)
    temp_gr.SetMarkerColor(c1)
    temp_gr.Draw("pe")
    grs += [temp_gr]

    temp_gr_stat = ROOT.TGraphErrors()
    temp_gr_stat.SetPoint(0, -2.5, 1.35 - 0.08*n)
    temp_gr_stat.SetPointError(0, 0.4, 0.0)
    temp_gr_stat.SetLineWidth(2)
    temp_gr_stat.SetLineColor(c2)
    temp_gr_stat.SetMarkerColor(c2)
    temp_gr_stat.Draw("pe")
    grs += [temp_gr_stat]

    label = ROOT.TLatex()
    label.SetTextFont(43)
    label.SetTextAlign(12)
    label.SetTextSize(28)
    label.SetTextColor(c1)
    label.DrawLatex(-1.25, 1.35 - 0.08*n, name)

    n += 1


label = ROOT.TLatex()
label.SetTextFont(43)
label.SetTextAlign(31)
label.SetTextSize(28)
label.SetTextColor(ROOT.kBlack)
label.DrawLatex(8.9, 1.41, "Run 2 ATLAS & CMS Hbb/cc results")

label = ROOT.TLatex()
label.SetTextFont(43)
label.SetTextAlign(31)
label.SetTextSize(28)
label.SetTextColor(ROOT.kGray+2)
label.DrawLatex(8.9, 1.45, date.today().strftime("%d/%m/%Y"))

pad1.cd()
label = ROOT.TLatex()
label.SetTextFont(53)
label.SetTextAlign(11)
label.SetTextSize(24)
label.SetTextColor(ROOT.kGray+2)
label.DrawLatex(-0.1, 1.41, "gitlab.cern.ch/mmuskinj/hbb-results")

canv.Print("hbb-results.pdf")
canv.Print("hbb-results.png")
