#!/usr/bin/python
import copy

data = {
    "VHbb-obs": {
        "data_fb": 80,
        "mu_sig": 1.16,
        "mu_sig_err_stat": [0.16, 0.16],
        "mu_sig_err_syst": [0.21, 0.19],
        "mu_sig_err_tot": [0.27, 0.25],
        "reference": "PLB 786 (2018) 59",
        "doi": "https://www.sciencedirect.com/science/article/pii/S0370269318307056",
        "order": 1,
    },
    "VHbb-Run2": {
        "data_fb": 139,
        "data_years": [2015, 2016, 2017, 2018],
        "mu_sig": 1.02,
        "mu_sig_err_stat": [0.12, 0.11],
        "mu_sig_err_syst": [0.14, 0.13],
        "mu_sig_err_tot": [0.18, 0.17],
        "significance_obs": 6.7,
        "significance_exp": 6.7,
        "reference": "EPJC 81 (2021) 178",
        "doi": "https://link.springer.com/article/10.1140/epjc/s10052-020-08677-2",
        "order": 2,
    },
    "ttHbb-2016": {
        "data_fb": 36.1,
        "data_years": [2015, 2016],
        "mu_sig": 0.84,
        "mu_sig_err_stat": [0.29, 0.29],
        "mu_sig_err_syst": [0.57, 0.54],
        "mu_sig_err_tot": [0.64, 0.61],
        "reference": "PRD 97 (2018) 072016",
        "doi": "https://journals.aps.org/prd/abstract/10.1103/PhysRevD.97.072016",
        "order": 5,
    },
    "ttHbb-Run2": {
        "data_fb": 139,
        "data_years": [2015, 2016, 2017, 2018],
        "mu_sig": 0.35,
        "mu_sig_err_stat": [0.20, 0.20],
        "mu_sig_err_syst": [0.30, 0.28],
        "mu_sig_err_tot": [0.36, 0.34],
        "reference": "JHEP 06 (2022) 97",
        "doi": "https://link.springer.com/article/10.1007/JHEP06(2022)097",
        "order": 8,
    },
    "VHcc-2016": {
        "data_fb": 36.1,
        "data_years": [2015, 2016],
        "mu_sig": -69,
        "mu_sig_err_stat": None,
        "mu_sig_err_syst": None,
        "mu_sig_err_tot": [101, 101],
        "reference": "PRL 120 (2018) 211802",
        "doi": "https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.120.211802",
        "order": 12,
    },
    "VHcc-Run2": {
        "data_fb": 139,
        "data_years": [2015, 2016, 2017, 2018],
        "mu_sig": -9,
        "mu_sig_err_stat": [10, 10],
        "mu_sig_err_syst": [12, 12],
        "mu_sig_err_tot": [15.6, 15.6],
        "reference": "EPJC 82 (2022) 717",
        "doi": "https://link.springer.com/article/10.1140/epjc/s10052-022-10588-3",
        "order": 13,
    },
    "VBF-Hbb": {
        "data_fb": 126,
        "mu_sig": 0.95,
        "mu_sig_err_stat": [0.32, 0.32],
        "mu_sig_err_syst": [0.20, 0.17],
        "mu_sig_err_tot": [0.38, 0.36],
        "reference": "EPJC 81 (2021) 537",
        "doi": "https://link.springer.com/article/10.1140/epjc/s10052-021-09192-8",
        "order": 4,
    },
    "Hbb-boosted-Run2": {
        "data_fb": 136,
        "mu_sig": 0.8,
        "mu_sig_err_stat": None,
        "mu_sig_err_syst": None,
        "mu_sig_err_tot": [3.2, 3.2],
        "reference": "PRD 105 (2022) 092003",
        "doi": "https://journals.aps.org/prd/abstract/10.1103/PhysRevD.105.092003",
        "order": 10,
    },
}

# From the Hbb fit
data["VHbb-Run2-Hbb"] = copy.deepcopy(data["VHbb-Run2"])
data["VHbb-Run2-Hbb"]["mu_sig"] = 1.17
data["VHbb-Run2-Hbb"]["mu_sig_err_stat"] =[0.16, 0.16]
data["VHbb-Run2-Hbb"]["mu_sig_err_syst"] = [0.19, 0.16]
data["VHbb-Run2-Hbb"]["mu_sig_err_tot"] = [0.25, 0.23]
data["VHbb-Run2-Hbb"]["significance_obs"] = 5.5
data["VHbb-Run2-Hbb"]["significance_exp"] = 4.9
data["VHbb-Run2-Hbb"]["order"] = 3
