# Hbb-results

Latest version of the plot below.

![hbb-results](https://gitlab.cern.ch/mmuskinj/hbb-results/-/jobs/artifacts/ci/raw/hbb-results.png?job=build){width=50%}

Download:
- [hbb-results.pdf](https://gitlab.cern.ch/mmuskinj/hbb-results/-/jobs/artifacts/ci/raw/hbb-results.pdf?job=build)
- [hbb-results.png](https://gitlab.cern.ch/mmuskinj/hbb-results/-/jobs/artifacts/ci/raw/hbb-results.png?job=build)
